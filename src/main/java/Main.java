import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

    }

    public static int[] transformArray(int... array) throws RuntimeException {
        if (array.length == 0) {
            throw new RuntimeException("Массив пустой!");
        } else {
            for (int i = array.length - 1; i >= 0; i--) {
                if (array[i] == 4) {
                    int[] newArray = new int[array.length - i - 1];
                    System.arraycopy(array, (i + 1), newArray, 0, (array.length - i - 1));
                    System.out.println(Arrays.toString(newArray));
                    return newArray;
                }
            }
        }
        throw new RuntimeException("В массиве нет ни одной 4!");
    }

    public static boolean containsOneAndFour(int... array) {
        boolean flag = false;
        int count1 = 0;
        int count4 = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 1) {
                count1++;
            }
            if (array[i] == 4) {
                count4++;
            }
            if ((count1 > 0) && (count4 > 0) && (count1 + count4 == array.length)) {
                flag = true;
                System.out.println("Успешно! Массив содержит только 1 и 4!");
            }
        }
        return flag;
    }
}



