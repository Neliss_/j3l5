import org.junit.Assert;
import org.junit.Test;

public class TestsOfArrays {
    Main main = new Main();

    @Test
    public void testTransformArray(){
        Assert.assertArrayEquals(new int[]{1,7}, main.transformArray(1,2,4,4,2,3,4,1,7));
    }
    @Test
    public void testTransformArray1(){
        Assert.assertArrayEquals(new int[]{5,6,7,8}, main.transformArray(1,2,3,4,5,6,7,8));
    }
    @Test
    public void testTransformArray2(){
        Assert.assertArrayEquals(new int[]{4}, main.transformArray(1,2,3));
    }
    @Test
    public void testTransformArray3(){
        Assert.assertArrayEquals(new int[]{0}, main.transformArray(4,4,4,4,4,0));
    }

    @Test
    public void testContainsOneAndFour(){
        Assert.assertTrue(main.containsOneAndFour(new int[]{1,1,1,1,1,4,4,4,1,4}));
    }

    @Test
    public void testContainsOneAndFour1(){
        Assert.assertTrue(main.containsOneAndFour(new int[]{1,1,1,1}));
    }

    @Test
    public void testContainsOneAndFour2(){
        Assert.assertTrue(main.containsOneAndFour(new int[]{4,4,4,4,4}));
    }

    @Test
    public void testContainsOneAndFour3(){
        Assert.assertTrue(main.containsOneAndFour(new int[]{1,4,4,1,1,4,3}));
    }


}
